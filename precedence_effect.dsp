/*Precedence Effect*/
import("stdfaust.lib");

room_width = hslider("ritardo", 400, 0, 4410,1);

process = hgroup("Precedence effect",(_ : fi.allpass_comb(64,16,1) <: _, de.delay(44100,room_width)));
